<?php

namespace app\controllers;

use Yii;
use app\models\ExpertTest;
use app\models\ExpertTestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;

/**
 * ExpertTestController implements the CRUD actions for ExpertTest model.
 */
class ExpertTestController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ExpertTest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ExpertTestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ExpertTest model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ExpertTest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ExpertTest();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ExpertTest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ExpertTest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ExpertTest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ExpertTest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ExpertTest::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionTest()
    {
        $model = new ExpertTest();
        if (Yii::$app->request->isPost) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $count_all = ExpertTest::find()->count();
            $count_pos = ExpertTest::find()->where(['disease' => 'positive'])->count();
            $count_neg = ExpertTest::find()->where(['disease' => 'negative'])->count();
            $ages = ExpertTest::find('age')->all();


            return [
                'count all' => $count_all,
                'count positive' => $count_pos,
                'count negative' => $count_neg,
                'ages' => $ages
            ];
        }

        return $this->render('test', [
            'model' => $model,
        ]);
    }

    public function actionBayesTest()
    {
        $model = new ExpertTest();

        if (Yii::$app->request->isPost)
        {
            //اسناد القيم المدخلة من قبل المسخدم الى متحولات
            $age = (int) Yii::$app->request->post()['ExpertTest']['age'];
            $chest_pain_type = Yii::$app->request->post()['ExpertTest']['chest_pain_type'];
            $rest_blood_pressure = (int) Yii::$app->request->post()['ExpertTest']['rest_blood_pressure'];
            $blood_sugar = Yii::$app->request->post()['ExpertTest']['blood_sugar'];
            $rest_electro = Yii::$app->request->post()['ExpertTest']['rest_electro'];
            $max_heart_rate = (int) Yii::$app->request->post()['ExpertTest']['max_heart_rate'];
            $exercice_angina = Yii::$app->request->post()['ExpertTest']['exercice_angina'];

            //مصفوفة تحوي جميع بيانات التدريب المخزنة بقاعدة البيانات
            $samples = ExpertTest::find()->all();
            
            //ايجاد عدد أمثلة التدريب
            $all_count = ExpertTest::find()->count();
            $pos_count = ExpertTest::find()->where(['disease' => 'positive'])->count();
            $neg_count = ExpertTest::find()->where(['disease' => 'negative'])->count();

            //ايجاد عدد أمثلة التدريب بالنسبة للبيانات النصية لكل من القيم الموجبة و السالبة
            $chest_pain_type_pos_count = ExpertTest::find()
                ->where(['disease' => 'positive'])
                ->andWhere(['chest_pain_type' => $chest_pain_type])
                ->count();
            $chest_pain_type_neg_count = ExpertTest::find()
                ->where(['disease' => 'negative'])
                ->andWhere(['chest_pain_type' => $chest_pain_type])
                ->count();

            $blood_sugar_pos_count = ExpertTest::find()
                ->where(['disease' => 'positive'])
                ->andWhere(['blood_sugar' => $blood_sugar])
                ->count();
            $blood_sugar_neg_count = ExpertTest::find()
                ->where(['disease' => 'negative'])
                ->andWhere(['blood_sugar' => $blood_sugar])
                ->count();

            $rest_electro_pos_count = ExpertTest::find()
                ->where(['disease' => 'positive'])
                ->andWhere(['rest_electro' => $rest_electro])
                ->count();
            $rest_electro_neg_count = ExpertTest::find()
                ->where(['disease' => 'negative'])
                ->andWhere(['rest_electro' => $rest_electro])
                ->count();

            $exercice_angina_pos_count = ExpertTest::find()
                ->where(['disease' => 'positive'])
                ->andWhere(['exercice_angina' => $exercice_angina])
                ->count();
            $exercice_angina_neg_count = ExpertTest::find()
                ->where(['disease' => 'negative'])
                ->andWhere(['exercice_angina' => $exercice_angina])
                ->count();

            $age_pos = [];
            $age_neg = [];
            
            $rest_blood_pressure_pos = [];
            $rest_blood_pressure_neg = [];
            
            
            $max_heart_rate_pos = [];
            $max_heart_rate_neg = [];

            //ايجاد عدد أمثلة التدريب بالنسبة للبيانات العددية لكل من القيم الموجبة و السالبة
            foreach ($samples as $sample) {
                if($sample->disease == 'positive')
                {
                    $age_pos[] = $sample->age;
                    $rest_blood_pressure_pos[] = $sample->rest_blood_pressure;
                    $max_heart_rate_pos[] = $sample->max_heart_rate;
                }
                else {
                    $age_neg[] = $sample->age;
                    $rest_blood_pressure_neg[] = $sample->rest_blood_pressure;
                    $max_heart_rate_neg[] = $sample->max_heart_rate;
                }
            }
            //حساب الكثافة من أجل القيم العددية
            $age_pos_dens = ExpertTest::density($age_pos, $age);
            $age_neg_dens = ExpertTest::density($age_neg, $age);
    
            $rest_blood_pressure_pos_dens = ExpertTest::density($rest_blood_pressure_pos,
                $rest_blood_pressure);
            $rest_blood_pressure_neg_dens = ExpertTest::density($rest_blood_pressure_neg,
                $rest_blood_pressure);
    
            $max_heart_rate_pos_dens = ExpertTest::density($max_heart_rate_pos, $max_heart_rate);
            $max_heart_rate_neg_dens = ExpertTest::density($max_heart_rate_neg , $max_heart_rate);
            
            //bayesايجاد احتمال كل من الموجب و السالب حسب معادلة
            $probability_pos = ($age_pos_dens) *
                                ($chest_pain_type_pos_count / $pos_count) *
                                ($rest_blood_pressure_pos_dens) *
                                ($blood_sugar_pos_count / $pos_count) * 
                                ($rest_electro_pos_count / $pos_count) * 
                                ($max_heart_rate_pos_dens) * 
                                ($exercice_angina_pos_count / $pos_count) * 
                                ($pos_count / $all_count);

            $probability_neg = ($age_neg_dens) * 
                                ($chest_pain_type_neg_count / $neg_count) * 
                                ($rest_blood_pressure_neg_dens) * 
                                ($blood_sugar_neg_count / $neg_count) * 
                                ($rest_electro_neg_count / $neg_count) * 
                                ($max_heart_rate_neg_dens) * 
                                ($exercice_angina_neg_count / $neg_count) * 
                                ($neg_count / $all_count);

            $positive_result = (float) (($probability_pos / ($probability_pos + $probability_neg)) * 100);
            $negative_result = (float) (($probability_neg / ($probability_neg + $probability_pos)) * 100);

            $result = ($positive_result > $negative_result)? 'Positive' : 'Negative';

            return $this->render('bayes-result', [
                'positive' => round($positive_result, 2),
                'negative' => round($negative_result,2),
                'result' => $result
            ]);
            
        }
        
        return $this->render('expert-test', [
            'model' => $model,
        ]);
    }

    public function actionId3Test()
    {
        //hard-coded اسناد قيم ابتدائية للمدخلات 
        $age = 40;
        $chest_pain_type = 'atyp_angina';
        $rest_blood_prusure = 133;
        $blood_sugar = 'TRUE';
        $rest_electro = 'atyp_angina';
        $max_heart_rate = 185;
        $exercice_angina = 'yes';

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        //مصفوفة تحوي جميع بيانات التدريب المخزنة بقاعدة البيانات
        $samples_model = ExpertTest::find()->all();
        
        //ايجاد مجموعة القيم الممكنة لكل مدخل
        //و حساب المجالات للقيم العددية
        $age_factors = ExpertTest::factors($samples_model, 'age');
        $chest_pain_type_values = Yii::$app->db->createCommand("select distinct chest_pain_type from heart_disease_samples")->queryAll();
        $rest_blood_pressure_factors = ExpertTest::factors($samples_model, 'rest_blood_pressure');
        $blood_sugar_values = Yii::$app->db->createCommand("select distinct blood_sugar from heart_disease_samples")->queryAll();
        $rest_electro_values = Yii::$app->db->createCommand("select distinct rest_electro from heart_disease_samples")->queryAll();
        $max_heart_rate_factors = ExpertTest::factors($samples_model, 'max_heart_rate');
        $exercice_angina_values = Yii::$app->db->createCommand("select distinct exercice_angina from heart_disease_samples")->queryAll();

        //حساب عدد كل من الاحتمالات السالبة و الموجبة لكل قيمة من كل مدخل على حدى 
        //و وضعها ضمن مجموعة ليتم حساب الانتروبي و الربح لكل صفة
        $positive_count = count(array_filter($samples_model, function($sample) {return $sample['disease'] == 'positive';}));
        $negative_count = count(array_filter($samples_model, function($sample) {return $sample['disease'] == 'negative';}));
        $current_set = ['pos' => $positive_count, 'neg' => $negative_count];

        $entropy = (-1*( ($positive_count/($positive_count+$negative_count)) * log(($positive_count/($positive_count+$negative_count))))) - (($negative_count/($negative_count+$positive_count))*log(($negative_count/($negative_count+$positive_count))));

        $age_positive_1 = count(array_filter($samples_model, function($sample) use ($age_factors) {return $sample['disease'] == 'positive' && $sample['age'] <= $age_factors[0];}));
        $age_positive_2 = count(array_filter($samples_model, function($sample) use ($age_factors) {return $sample['disease'] == 'positive' && $sample['age'] <= $age_factors[1] && $sample['age'] > $age_factors[0];}));
        $age_positive_3 = count(array_filter($samples_model, function($sample) use ($age_factors) {return $sample['disease'] == 'positive' && $sample['age'] <= $age_factors[2] && $sample['age'] > $age_factors[1];}));
        $age_positive_4 = count(array_filter($samples_model, function($sample) use ($age_factors) {return $sample['disease'] == 'positive' && $sample['age'] <= $age_factors[3] && $sample['age'] > $age_factors[2];}));
        $age_negative_1 = count(array_filter($samples_model, function($sample) use ($age_factors) {return $sample['disease'] == 'negative' && $sample['age'] <= $age_factors[0];}));
        $age_negative_2 = count(array_filter($samples_model, function($sample) use ($age_factors) {return $sample['disease'] == 'negative' && $sample['age'] <= $age_factors[1] && $sample['age'] > $age_factors[0];}));
        $age_negative_3 = count(array_filter($samples_model, function($sample) use ($age_factors) {return $sample['disease'] == 'negative' && $sample['age'] <= $age_factors[2] && $sample['age'] > $age_factors[1];}));
        $age_negative_4 = count(array_filter($samples_model, function($sample) use ($age_factors) {return $sample['disease'] == 'negative' && $sample['age'] <= $age_factors[3] && $sample['age'] > $age_factors[2];}));
        $age_set = [['pos' => $age_positive_1, 'neg' => $age_negative_1], ['pos' => $age_positive_2, 'neg' => $age_negative_2], ['pos' => $age_positive_3, 'neg' => $age_negative_3], ['pos' => $age_positive_4, 'neg' => $age_negative_4]];

        $chest_pain_type_positive_1 = count(array_filter($samples_model, function($sample) use ($chest_pain_type_values) {return $sample['disease'] == 'positive' && $sample['chest_pain_type'] == $chest_pain_type_values[0]['chest_pain_type'];}));
        $chest_pain_type_positive_2 = count(array_filter($samples_model, function($sample) use ($chest_pain_type_values) {return $sample['disease'] == 'positive' && $sample['chest_pain_type'] == $chest_pain_type_values[1]['chest_pain_type'];}));
        $chest_pain_type_positive_3 = count(array_filter($samples_model, function($sample) use ($chest_pain_type_values) {return $sample['disease'] == 'positive' && $sample['chest_pain_type'] == $chest_pain_type_values[2]['chest_pain_type'];}));
        $chest_pain_type_positive_4 = count(array_filter($samples_model, function($sample) use ($chest_pain_type_values) {return $sample['disease'] == 'positive' && $sample['chest_pain_type'] == $chest_pain_type_values[3]['chest_pain_type'];}));
        $chest_pain_type_negative_1 = count(array_filter($samples_model, function($sample) use ($chest_pain_type_values) {return $sample['disease'] == 'negative' && $sample['chest_pain_type'] == $chest_pain_type_values[0]['chest_pain_type'];}));
        $chest_pain_type_negative_2 = count(array_filter($samples_model, function($sample) use ($chest_pain_type_values) {return $sample['disease'] == 'negative' && $sample['chest_pain_type'] == $chest_pain_type_values[1]['chest_pain_type'];}));
        $chest_pain_type_negative_3 = count(array_filter($samples_model, function($sample) use ($chest_pain_type_values) {return $sample['disease'] == 'negative' && $sample['chest_pain_type'] == $chest_pain_type_values[2]['chest_pain_type'];}));
        $chest_pain_type_negative_4 = count(array_filter($samples_model, function($sample) use ($chest_pain_type_values) {return $sample['disease'] == 'negative' && $sample['chest_pain_type'] == $chest_pain_type_values[3]['chest_pain_type'];}));
        $chest_pain_type_set = [['pos' => $chest_pain_type_positive_1, 'neg' => $chest_pain_type_negative_1], ['pos' => $chest_pain_type_positive_2, 'neg' => $chest_pain_type_negative_2], ['pos' => $chest_pain_type_positive_3, 'neg' => $chest_pain_type_negative_3], ['pos' => $chest_pain_type_positive_4, 'neg' => $chest_pain_type_negative_4]];

        $rest_blood_pressure_positive_1 = count(array_filter($samples_model, function($sample) use ($rest_blood_pressure_factors) {return $sample['disease'] == 'positive' && $sample['rest_blood_pressure'] <= $rest_blood_pressure_factors[0];}));
        $rest_blood_pressure_positive_2 = count(array_filter($samples_model, function($sample) use ($rest_blood_pressure_factors) {return $sample['disease'] == 'positive' && $sample['rest_blood_pressure'] <= $rest_blood_pressure_factors[1] && $sample['rest_blood_pressure'] > $rest_blood_pressure_factors[0];}));
        $rest_blood_pressure_positive_3 = count(array_filter($samples_model, function($sample) use ($rest_blood_pressure_factors) {return $sample['disease'] == 'positive' && $sample['rest_blood_pressure'] <= $rest_blood_pressure_factors[2] && $sample['rest_blood_pressure'] > $rest_blood_pressure_factors[1];}));
        $rest_blood_pressure_positive_4 = count(array_filter($samples_model, function($sample) use ($rest_blood_pressure_factors) {return $sample['disease'] == 'positive' && $sample['rest_blood_pressure'] <= $rest_blood_pressure_factors[3] && $sample['rest_blood_pressure'] > $rest_blood_pressure_factors[2];}));
        $rest_blood_pressure_negative_1 = count(array_filter($samples_model, function($sample) use ($rest_blood_pressure_factors) {return $sample['disease'] == 'negative' && $sample['rest_blood_pressure'] <= $rest_blood_pressure_factors[0];}));
        $rest_blood_pressure_negative_2 = count(array_filter($samples_model, function($sample) use ($rest_blood_pressure_factors) {return $sample['disease'] == 'negative' && $sample['rest_blood_pressure'] <= $rest_blood_pressure_factors[1] && $sample['rest_blood_pressure'] > $rest_blood_pressure_factors[0];}));
        $rest_blood_pressure_negative_3 = count(array_filter($samples_model, function($sample) use ($rest_blood_pressure_factors) {return $sample['disease'] == 'negative' && $sample['rest_blood_pressure'] <= $rest_blood_pressure_factors[2] && $sample['rest_blood_pressure'] > $rest_blood_pressure_factors[1];}));
        $rest_blood_pressure_negative_4 = count(array_filter($samples_model, function($sample) use ($rest_blood_pressure_factors) {return $sample['disease'] == 'negative' && $sample['rest_blood_pressure'] <= $rest_blood_pressure_factors[3] && $sample['rest_blood_pressure'] > $rest_blood_pressure_factors[2];}));
        $rest_blood_pressure_set = [['pos' => $rest_blood_pressure_positive_1, 'neg' => $rest_blood_pressure_negative_1], ['pos' => $rest_blood_pressure_positive_2, 'neg' => $rest_blood_pressure_negative_2], ['pos' => $rest_blood_pressure_positive_3, 'neg' => $rest_blood_pressure_negative_3], ['pos' => $rest_blood_pressure_positive_4, 'neg' => $rest_blood_pressure_negative_4]];

        $blood_sugar_positive_1 = count(array_filter($samples_model, function($sample) use ($blood_sugar_values) {return $sample['disease'] == 'positive' && $sample['blood_sugar'] == $blood_sugar_values[0]['blood_sugar'];}));
        $blood_sugar_positive_2 = count(array_filter($samples_model, function($sample) use ($blood_sugar_values) {return $sample['disease'] == 'positive' && $sample['blood_sugar'] == $blood_sugar_values[1]['blood_sugar'];}));
        $blood_sugar_negative_1 = count(array_filter($samples_model, function($sample) use ($blood_sugar_values) {return $sample['disease'] == 'negative' && $sample['blood_sugar'] == $blood_sugar_values[0]['blood_sugar'];}));
        $blood_sugar_negative_2 = count(array_filter($samples_model, function($sample) use ($blood_sugar_values) {return $sample['disease'] == 'negative' && $sample['blood_sugar'] == $blood_sugar_values[1]['blood_sugar'];}));
        $blood_sugar_set = [['pos' => $blood_sugar_positive_1, 'neg' => $blood_sugar_negative_1], ['pos' => $blood_sugar_positive_2, 'neg' => $blood_sugar_negative_2]];

        $rest_electro_positive_1 = count(array_filter($samples_model, function($sample) use ($rest_electro_values) {return $sample['disease'] == 'positive' && $sample['rest_electro'] == $rest_electro_values[0]['rest_electro'];}));
        $rest_electro_positive_2 = count(array_filter($samples_model, function($sample) use ($rest_electro_values) {return $sample['disease'] == 'positive' && $sample['rest_electro'] == $rest_electro_values[1]['rest_electro'];}));
        $rest_electro_positive_3 = count(array_filter($samples_model, function($sample) use ($rest_electro_values) {return $sample['disease'] == 'positive' && $sample['rest_electro'] == $rest_electro_values[2]['rest_electro'];}));
        $rest_electro_negative_1 = count(array_filter($samples_model, function($sample) use ($rest_electro_values) {return $sample['disease'] == 'negative' && $sample['rest_electro'] == $rest_electro_values[0]['rest_electro'];}));
        $rest_electro_negative_2 = count(array_filter($samples_model, function($sample) use ($rest_electro_values) {return $sample['disease'] == 'negative' && $sample['rest_electro'] == $rest_electro_values[1]['rest_electro'];}));
        $rest_electro_negative_3 = count(array_filter($samples_model, function($sample) use ($rest_electro_values) {return $sample['disease'] == 'negative' && $sample['rest_electro'] == $rest_electro_values[2]['rest_electro'];}));
        $rest_electro_set = [['pos' => $rest_electro_positive_1, 'neg' => $rest_electro_negative_1], ['pos' => $rest_electro_positive_2, 'neg' => $rest_electro_negative_2], ['pos' => $rest_electro_positive_3, 'neg' => $rest_electro_negative_3]];

        $max_heart_rate_positive_1 = count(array_filter($samples_model, function($sample) use ($max_heart_rate_factors) {return $sample['disease'] == 'positive' && $sample['max_heart_rate'] <= $max_heart_rate_factors[0];}));
        $max_heart_rate_positive_2 = count(array_filter($samples_model, function($sample) use ($max_heart_rate_factors) {return $sample['disease'] == 'positive' && $sample['max_heart_rate'] <= $max_heart_rate_factors[1] && $sample['max_heart_rate'] > $max_heart_rate_factors[0];}));
        $max_heart_rate_positive_3 = count(array_filter($samples_model, function($sample) use ($max_heart_rate_factors) {return $sample['disease'] == 'positive' && $sample['max_heart_rate'] <= $max_heart_rate_factors[2] && $sample['max_heart_rate'] > $max_heart_rate_factors[1];}));
        $max_heart_rate_positive_4 = count(array_filter($samples_model, function($sample) use ($max_heart_rate_factors) {return $sample['disease'] == 'positive' && $sample['max_heart_rate'] <= $max_heart_rate_factors[3] && $sample['max_heart_rate'] > $max_heart_rate_factors[2];}));
        $max_heart_rate_negative_1 = count(array_filter($samples_model, function($sample) use ($max_heart_rate_factors) {return $sample['disease'] == 'negative' && $sample['max_heart_rate'] <= $max_heart_rate_factors[0];}));
        $max_heart_rate_negative_2 = count(array_filter($samples_model, function($sample) use ($max_heart_rate_factors) {return $sample['disease'] == 'negative' && $sample['max_heart_rate'] <= $max_heart_rate_factors[1] && $sample['max_heart_rate'] > $max_heart_rate_factors[0];}));
        $max_heart_rate_negative_3 = count(array_filter($samples_model, function($sample) use ($max_heart_rate_factors) {return $sample['disease'] == 'negative' && $sample['max_heart_rate'] <= $max_heart_rate_factors[2] && $sample['max_heart_rate'] > $max_heart_rate_factors[1];}));
        $max_heart_rate_negative_4 = count(array_filter($samples_model, function($sample) use ($max_heart_rate_factors) {return $sample['disease'] == 'negative' && $sample['max_heart_rate'] <= $max_heart_rate_factors[3] && $sample['max_heart_rate'] > $max_heart_rate_factors[2];}));
        $max_heart_rate_set = [['pos' => $max_heart_rate_positive_1, 'neg' => $max_heart_rate_negative_1], ['pos' => $max_heart_rate_positive_2, 'neg' => $max_heart_rate_negative_2], ['pos' => $max_heart_rate_positive_3, 'neg' => $max_heart_rate_negative_3], ['pos' => $max_heart_rate_positive_4, 'neg' => $max_heart_rate_negative_4]];

        $exercice_angina_positive_1 = count(array_filter($samples_model, function($sample) use ($exercice_angina_values) {return $sample['disease'] == 'positive' && $sample['exercice_angina'] == $exercice_angina_values[0]['exercice_angina'];}));
        $exercice_angina_positive_2 = count(array_filter($samples_model, function($sample) use ($exercice_angina_values) {return $sample['disease'] == 'positive' && $sample['exercice_angina'] == $exercice_angina_values[1]['exercice_angina'];}));
        $exercice_angina_negative_1 = count(array_filter($samples_model, function($sample) use ($exercice_angina_values) {return $sample['disease'] == 'negative' && $sample['exercice_angina'] == $exercice_angina_values[0]['exercice_angina'];}));
        $exercice_angina_negative_2 = count(array_filter($samples_model, function($sample) use ($exercice_angina_values) {return $sample['disease'] == 'negative' && $sample['exercice_angina'] == $exercice_angina_values[1]['exercice_angina'];}));
        $exercice_angina_set = [['pos' => $exercice_angina_positive_1, 'neg' => $exercice_angina_negative_1], ['pos' => $exercice_angina_positive_2, 'neg' => $exercice_angina_negative_2]];

        //حساب الربح لكل عقدة 
        $nodes_gain = [];
        $nodes_gain['age'] = ExpertTest::gain($current_set, $age_set);
        $nodes_gain['chest_pain_type'] = ExpertTest::gain($current_set, $chest_pain_type_set);
        $nodes_gain['rest_blood_pressure'] = ExpertTest::gain($current_set, $rest_blood_pressure_set);
        $nodes_gain['blood_sugar'] = ExpertTest::gain($current_set, $blood_sugar_set);
        $nodes_gain['rest_electro'] = ExpertTest::gain($current_set, $rest_electro_set);
        $nodes_gain['max_heart_rate'] = ExpertTest::gain($current_set, $max_heart_rate_set);
        $nodes_gain['exercice_angina'] = ExpertTest::gain($current_set, $exercice_angina_set);

        //اختيار أكبر ربح
        $best_gain = max($nodes_gain);
        
        return [
            'Best Gain' => $best_gain,
        ];
    }
}
