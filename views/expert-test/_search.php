<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ExpertTestSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="expert-test-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'age') ?>

    <?= $form->field($model, 'chest_pain_type') ?>

    <?= $form->field($model, 'rest_blood_pressure') ?>

    <?= $form->field($model, 'blood_sugar') ?>

    <?php // echo $form->field($model, 'rest_electro') ?>

    <?php // echo $form->field($model, 'max_heart_rate') ?>

    <?php // echo $form->field($model, 'exercice_angina') ?>

    <?php // echo $form->field($model, 'disease') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
