<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ExpertTest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="expert-test-form _form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'age')->textInput(['type' => 'number', 'min' => 18, 'max' => 110]) ?>

    <?= $form->field($model, 'chest_pain_type')->dropDownList([ 'asympt' => 'Asympt', 'atyp_angina' => 'Atyp angina', 'non_anginal' => 'Non anginal', 'typ_angina' => 'Typ angina', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'rest_blood_pressure')->textInput(['type' => 'number', 'min' => 60, 'max' => 250]) ?>

    <?= $form->field($model, 'blood_sugar')->dropDownList([ 'TRUE' => 'TRUE', 'FALSE' => 'FALSE', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'rest_electro')->dropDownList([ 'normal' => 'Normal', 'left_vent_hyper' => 'Left vent hyper', 'st_t_wave_abnormality' => 'St t wave abnormality', 'not_set' => 'Not set', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'max_heart_rate')->textInput(['type' => 'number', 'min' => 60, 'max' => 250]) ?>

    <?= $form->field($model, 'exercice_angina')->dropDownList([ 'yes' => 'Yes', 'no' => 'No', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
