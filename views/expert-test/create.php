<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ExpertTest */

$this->title = Yii::t('app', 'Create Expert Test');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Expert Tests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expert-test-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
