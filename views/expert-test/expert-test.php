<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Expert Heart Test');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expert-test-create">

<h1><?= Html::encode($this->title) ?></h1>
<h3>Please fill in the following information to perform an online expert heart test:</h3>
<hr>

<div class="expert-test-form _form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'age')->textInput(['type' => 'number', 'min' => 18, 'max' => 110]) ?>

    <?= $form->field($model, 'chest_pain_type')->dropDownList([
        'asympt' => 'Asympt',
        'atyp_angina' => 'Atyp angina',
        'non_anginal' => 'Non anginal',
        'typ_angina' => 'Typ angina', 
        ],
        ['prompt' => ''])
    ?>

    <?= $form->field($model, 'rest_blood_pressure')->textInput([
        'type' => 'number', 'min' => 60, 'max' => 250
        ]) ?>

    <?= $form->field($model, 'blood_sugar')->dropDownList([
        'TRUE' => 'TRUE', 'FALSE' => 'FALSE', 
        ],
        ['prompt' => '']) ?>

    <?= $form->field($model, 'rest_electro')->dropDownList([
        'normal' => 'Normal',
        'left_vent_hyper' => 'Left vent hyper',
        'st_t_wave_abnormality' => 'St t wave abnormality'
        ], 
        ['prompt' => '']) ?>

    <?= $form->field($model, 'max_heart_rate')->textInput([
        'type' => 'number', 'min' => 60, 'max' => 250
        ]) ?>

    <?= $form->field($model, 'exercice_angina')->dropDownList([
        'yes' => 'Yes', 'no' => 'No'
        ],
        ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>