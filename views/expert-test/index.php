<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ExpertTestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Expert Tests');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expert-test-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Expert Test'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'age',
            'chest_pain_type',
            'rest_blood_pressure',
            'blood_sugar',
            //'rest_electro',
            //'max_heart_rate',
            //'exercice_angina',
            //'disease',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
