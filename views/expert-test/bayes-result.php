<?php

use yii\helpers\Html;

?>

<div>
    <h1>Result:</h1>
    <h3>Your result according to Naive Bayes Algorithm is:</h3>
    <h3>Positive: <?= $positive ?>% and Negative: <?= $negative ?>%</h3> 
    <br>
    <h2>Your test result is: <?= ($result == 'Positive' ? "<span style='color:red'>" . $result. "</span>" : "<span style='color:green'>" . $result. "</span>");$result ?></h2>
    <?= Html::a(Yii::t('app', 'Redo Test'), ['expert-test/bayes-test'], ['class' => 'btn btn-primary']) ?>
</div>
