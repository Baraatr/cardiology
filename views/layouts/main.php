<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

$items = [];

$items[] = ['label' => 'Expert Heart Test', 'url' => ['/expert-test/bayes-test'], 'linkOptions' => ['style' => 'color: black']];

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/iconfinder_cardiology_44697.png',['id' => 'logo']) . ' ' . '<span>Cardiology Clinic</span>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'my-nav navbar-fixed-top',
        ]
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => $items
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">

        <p class="">by: Baraa Tanjour, Heba Arroub</p>
    </div>
</footer>

<?php $this->endBody() ?>

<script>
$(document).ready(function(){
    $('.consultation_reply').click(function(e){
        e.preventDefault();
        $('#consultation_reply_modal').modal('show')
            .find('#consultation_reply_modal_content')
            .load($(this).attr('value'));
    });
});
</script>

</body>
</html>
<?php $this->endPage() ?>

