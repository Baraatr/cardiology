<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Cardiac Clinic';
?>

<div class="site-index">

    <div class="jumbotron">
        <h1>Cardiology Clinck</h1>

        <p class="lead">Welcome to my web application.You can make a heart test to see if you have disease or not.</p>

        <p><?= Html::a(Yii::t('app', 'Start The Test'), ['/expert-test/bayes-test'], ['class' => 'btn btn-lg btn-default']) ?></p>
    </div>
</div>
