<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ExpertTest;

/**
 * ExpertTestSearch represents the model behind the search form of `app\models\ExpertTest`.
 */
class ExpertTestSearch extends ExpertTest
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'age', 'rest_blood_pressure', 'max_heart_rate'], 'integer'],
            [['chest_pain_type', 'blood_sugar', 'rest_electro', 'exercice_angina', 'disease'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ExpertTest::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'age' => $this->age,
            'rest_blood_pressure' => $this->rest_blood_pressure,
            'max_heart_rate' => $this->max_heart_rate,
        ]);

        $query->andFilterWhere(['like', 'chest_pain_type', $this->chest_pain_type])
            ->andFilterWhere(['like', 'blood_sugar', $this->blood_sugar])
            ->andFilterWhere(['like', 'rest_electro', $this->rest_electro])
            ->andFilterWhere(['like', 'exercice_angina', $this->exercice_angina])
            ->andFilterWhere(['like', 'disease', $this->disease]);

        return $dataProvider;
    }
}
