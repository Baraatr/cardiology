<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "heart_disease_samples".
 *
 * @property int $id
 * @property int $age
 * @property string $chest_pain_type
 * @property int $rest_blood_pressure
 * @property string $blood_sugar
 * @property string|null $rest_electro
 * @property int $max_heart_rate
 * @property string $exercice_angina
 * @property string $disease
 */
class ExpertTest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    //array:مصفوفة الأعداد المطلوب حساب الانحراف المعياري لها
    //mean:المتوسط الحسابي لعناصر المصفوفة
    static public function sd($array, $mean)
    {   
        foreach ($array as $index => $value) {
            $array[$index] = pow($value-$mean, 2);
        }
        return (array_sum($array)/(count($array)-1));
    }
    //val:القيمة المدخلة المراد حساب الكثافة من أجلها
    static public function density($array, $val)
    {
        $mean = (array_sum($array) / count($array));
        $sd = ExpertTest::sd($array, $mean);
        $d = (1/sqrt(2*M_PI*$sd))*pow(M_E, (-1*(pow(($val-$mean), 2)/(2*pow($sd, 2)))));

        return $d;
    }
    
    //(factors) يتم تمرير مصفوفة بيانات التدريب و الصفة ذات البيانات الرقمية المراد تقسيمها الى مجالات
    static public function factors($samples, $property)
    {
        $factors = [];
        $property_values = [];
        foreach ($samples as $index => $sample) {
            $property_values[] = $sample[$property];
        }
        sort($property_values);
        $ranges = array_chunk($property_values, ceil(count($property_values)/4));
        for ($i=0; $i < count($ranges); $i++) { 
            $factors[$i] = $ranges[$i][count($ranges[$i])-1];
        }

        return $factors;
    }

    static public function entropy($set)
    {
        return (-1*(($set['pos']/($set['pos']+$set['neg'])) * log(($set['pos']/($set['pos']+$set['neg']))))) - (($set['neg']/($set['neg']+$set['pos']))*log(($set['neg']/($set['neg']+$set['pos']))));
    }

    static public function gain($current_set, $all_child_set)
    {
        $current_set_entropy = ExpertTest::entropy($current_set);
        $all_child_sets_entropy = 0;
        foreach ($all_child_set as $index => $set) {
            $set_entropy = ExpertTest::entropy($set);
            $all_child_sets_entropy += ($set['pos']+$set['neg'])/($current_set['pos']+$current_set['neg']) * $set_entropy;
        }
        $gain = $current_set_entropy - $all_child_sets_entropy;
        return $gain;
    }

    public static function tableName()
    {
        return 'heart_disease_samples';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //جميع القيم التالية مطلوب ادخالها من المستخدم
            [['age', 'chest_pain_type', 'rest_blood_pressure',
                'blood_sugar', 'rest_electro', 'max_heart_rate', 'exercice_angina'], 'required'],
            //جميع القيم التالية هي قيم رقمية
            [['age', 'rest_blood_pressure', 'max_heart_rate'], 'integer'],
            //جميع القيم التالية هي قيم نصية
            [['chest_pain_type', 'blood_sugar',
                'rest_electro', 'exercice_angina'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'age' => Yii::t('app', 'Age'),
            'chest_pain_type' => Yii::t('app', 'Chest Pain Type'),
            'rest_blood_pressure' => Yii::t('app', 'Rest Blood Pressure'),
            'blood_sugar' => Yii::t('app', 'Blood Sugar'),
            'rest_electro' => Yii::t('app', 'Rest Electro'),
            'max_heart_rate' => Yii::t('app', 'Max Heart Rate'),
            'exercice_angina' => Yii::t('app', 'Exercice Angina'),
            'disease' => Yii::t('app', 'Disease'),
        ];
    }
}
